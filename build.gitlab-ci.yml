---
# Build package with yocto
.build:component-yocto:
  image: $OSS_IMAGE
  tags: [docker]
  timeout: 4 hours
  stage: build
  interruptible: true
  rules:
    - if: '$CI_COMMIT_TAG && $PC_BUILD_COMP_YOCTO == "on"'
      when: manual
  retry:
    max: 2
    when:
      - runner_system_failure
      - api_failure
      - scheduler_failure
  variables:
    TEMPLATECONF: "meta-sah/tools-layers/meta-sah-prpl/conf/genericx86-64/"
  script:
    # Setup ssh keys and git config
    - mkdir -p $HOME/.ssh
    - |
      if [ -n "$SSH_PRIVATE_KEY" ]; then
        cp "$SSH_PRIVATE_KEY" $HOME/.ssh/id_rsa && chmod 0600 $HOME/.ssh/id_rsa
      fi
    - ssh-keyscan -H "$CI_SERVER_HOST" >> $HOME/.ssh/known_hosts
    - git config --global user.name "sahbot"
    - git config --global user.email "sahbot@softathome.com"
    # Prepare build environment
    - export WORKSPACEDIR="$HOME/workspaces"
    - mkdir -p $WORKSPACEDIR
    - cd $WORKSPACEDIR
    - repo init ${REPO_OPTIONS} -u ${MANIFEST_URL} -b ${MANIFEST_BRANCH} -m ${MANIFEST_FILE}
    - |
      if [ -n "$GITLAB_INTERNAL" ]; then
        sed -i "s~gitlab.com/soft.at.home~$GITLAB_INTERNAL~g" .repo/manifests/$MANIFEST_FILE
      else
        sed -i "s~ssh://git@~https://~g" .repo/manifests/$MANIFEST_FILE
      fi
    - repo sync
    - export TEMPLATECONF=$TEMPLATECONF
    - source oe-init-build-env
    - echo "GIT_BASE_URL = \"$GIT_BASE_URL\"" >> conf/local.conf
    - devtool modify -n $CI_PROJECT_NAME $CI_PROJECT_DIR
    - bitbake $CI_PROJECT_NAME

# Build debian package
.build:package:
  image: $SAH_CI_IMAGE
  tags: [docker]
  stage: build
  interruptible: true
  rules:
    - if: '$GITLAB_INTERNAL && $CI_COMMIT_REF_NAME == $INTERNAL_REF_BRANCH && $PC_BUILD_PACKAGE == "on" && $PC_BUILD_DEBIAN_PACKAGE == "on"'
      exists:
        - packages/package_debian/makefile
        - packages/package_debian/Makefile
        - ci/packaging/deb/Makefile
    - if: '$CI_COMMIT_TAG && $PC_BUILD_PACKAGE == "on" && $PC_BUILD_DEBIAN_PACKAGE == "on"'
      exists:
        - packages/package_debian/makefile
        - packages/package_debian/Makefile
        - ci/packaging/deb/Makefile
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $PC_BUILD_PACKAGE == "on" && $PC_BUILD_DEBIAN_PACKAGE == "on"'
      exists:
        - packages/package_debian/makefile
        - packages/package_debian/Makefile
        - ci/packaging/deb/Makefile
  retry:
    max: 2
    when:
      - runner_system_failure
      - api_failure
      - scheduler_failure
  script:
    - env
    - git fetch --tags
    - |
      if [[ -n "$COMPONENT_EXTRA_TOOLS" ]]; then
        for i in $(seq 1 5); do
          [ $i -gt 1 ] && sleep 15;
          sudo apt update && s=0 && break || s=$?;
        done; (exit $s)
        sudo apt -y install $COMPONENT_EXTRA_TOOLS;
      fi
    - |
      if [[ -n "$BUILD_DEPS" ]] || [[ -n "$PC_PACKAGE_DEPS" ]]; then
        for i in $(seq 1 5); do
          [ $i -gt 1 ] && sleep 15;
          sudo apt update && s=0 && break || s=$?;
        done; (exit $s)
        sudo apt -y install $BUILD_DEPS $PC_PACKAGE_DEPS;
      fi
    - |
      if [ -n "$CI_COMMIT_TAG" ] ; then
        export PACKAGE_VERSION=${CI_COMMIT_TAG//v}
      else
        export PACKAGE_VERSION=9.9.9
      fi
    - |
      if  [[ -f ci/packaging/deb/Makefile ]]; then
        mkdir -p "$STAGINGDIR"/include
        ln -sfr /components.config "$STAGINGDIR"/components.config
        ln -sfr /components.config.m4 "$STAGINGDIR"/components.config.m4
        ln -sfr /include/components.h "$STAGINGDIR"/include/components.h
        make && make install
        fakeroot make -C ci/packaging/deb PACKAGE_NAME="$PACKAGE_NAME" PACKAGE_VERSION="$PACKAGE_VERSION"
      else
        make package
      fi

.build:g++:
  image: $SAH_CI_IMAGE
  tags: [docker]
  stage: build
  interruptible: true
  rules:
    - if: '$PC_GPLUSPLUS == "off"'
      when: never
    - if: '$GITLAB_INTERNAL && $CI_COMMIT_REF_NAME == $INTERNAL_REF_BRANCH'
    - if: '$GITLAB_INTERNAL == null && $CI_COMMIT_REF_PROTECTED == "true"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$GITLAB_INTERNAL == null && $CI_COMMIT_TAG'
  retry:
    max: 2
    when:
      - runner_system_failure
      - api_failure
      - scheduler_failure
  script:
    - |
      if [[ -n "$BUILD_DEPS" ]] || [[ -n "$PC_PACKAGE_DEPS" ]]; then
        for i in $(seq 1 5); do
          [ $i -gt 1 ] && sleep 15;
          sudo apt update && s=0 && break || s=$?;
        done; (exit $s)
        sudo apt -y install $BUILD_DEPS $PC_PACKAGE_DEPS;
      fi
    - |
      tag=$(git describe --tags) || tag=""
      export VERSION_PREFIX=${tag%v*}
      export CC=g++
      echo "VERSION_PREFIX = " $VERSION_PREFIX
      make
  artifacts:
      paths:
        - $CI_PROJECT_DIR/$BLD_OUTPUT_DIR/

.build:documentation:
  image: $SAH_CI_IMAGE
  tags: [docker]
  stage: build
  interruptible: true
  rules:
    - if: '$PC_DOC_CHECK == "off"'
      when: never
    - if: '$GITLAB_INTERNAL && $CI_COMMIT_REF_NAME == $INTERNAL_REF_BRANCH'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$GITLAB_INTERNAL == null && $CI_COMMIT_TAG'
  retry:
    max: 2
    when:
      - runner_system_failure
      - api_failure
      - scheduler_failure
  script:
    - |
      for i in $(seq 1 5); do
        [ $i -gt 1 ] && sleep 15;
        sudo apt update && s=0 && break || s=$?;
      done; (exit $s)
      sudo apt -y install amxo-cg amxo-xml-to;
    - mkdir -p $CI_PROJECT_DIR/$BLD_OUTPUT_DIR/doc
    - |
      tag=$(git describe --tags) || tag=""
      export VERSION_PREFIX=${tag%v*}
      echo "VERSION_PREFIX = " $VERSION_PREFIX
    - |
      if eval make doc --dry-run &> /dev/null; then
        make doc
      fi
  artifacts:
      paths:
        - $CI_PROJECT_DIR/$BLD_OUTPUT_DIR/doc/$DOC_HTML_DIR/*
        - $CI_PROJECT_DIR/$BLD_OUTPUT_DIR/html/*
